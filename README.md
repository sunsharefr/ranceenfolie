**Jeu de plateau** familial à propos de la Rance en Bretagne, de son parc naturel et de ses environs. Ce jeu met en scène Joséphine le phoque de mordreuc.
Il a été réalisé pendant le confinement en Avril 2020 par des enfants. C'est une réplique du jeu (C) monopoly.

*Mesdames et messieurs,
Ce printemps, c’est la pandémie et les hommes ont quitté les bords de Rance. Youpi !!! Je suis libre d’aller dormir et me dorer la pilule au soleil dans tous
mes endroits préférés !! 
**#restezchezvous** et amusez vous bien avec ce jeu « Rance en folie » !!!
Joséphine (le phoque de Mordreuc)*

Venez (re)découvrir les lieux secrets du val de Rance.....en jouant à Rance en folie. Cales
de mouillage, malouinières, tentes de plage, barrage, forts, girouettes..... tout y est même le phoque de
Mordreuc. Amusez vous bien ! (les draft schpounk)

 ![image du jeu](https://gitlab.com/draftschpounk/ranceenfolie/-/blob/master/jeu_en_cours_m.JPG) 

Le dossier contient :
- le plateau de jeu et les cartes au format PDF et PNG
- les plans 3D de la cabane de plage, de la malouinière et des pions (lettres RANCE).  Il faut 40 tentes et 20 malouinieres

Il faut jouer avec deux dés.

Nous avons imprimé le plateau sur du tissu sur un site internet et ca marche bien (voir photo du plateau).
Il y a des kits de pions maison en bois sur les sites de vente de pions de jeux de société.

Contributeurs idéateurs : malo, hoel, tilouan, arthur, julien, manue